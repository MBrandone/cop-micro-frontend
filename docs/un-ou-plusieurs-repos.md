# Un ou plusieurs repository pour la CoP

Lundi 24 avril 2023

## Conclusion

> On fait du git sub modules

## Problématique

On veut exposer sur gitlab un exemple de refactor d'une application web SPA monolithe vers plusieurs micro-frontends.
Dans l'article, on pourra suivre le découpage à travers plusieurs commits.

## Détails
Les doutes :
1) Déploiements, comment ça se passe ?

Pour le déploiement du monolithe, on va déployer le front sur Vercel.
Pour le deploiement des MFE, on va déployer sur Vercel. Faudra pas lancer une pipeline automatique, à chaque commit, faudra voir quel MFE change.
Et dans ce cas

2) pipelines, comment ça se passe ?

Sur gitlab, on a un gitlab-ci pour le projet.
Le gitlab-ci devrait être lié au projet.
Dans les MFEs, on devrait pouvoir travailler sans se pré-occuper des autres repos. 
Donc un commit sur un MFE ne devrait pas lancé un build pour tout les autres MFE.

3) Partage de composants, et de librairies, comment ça se passe ?
en monorepo, je pense que c'est plus simple
En poly repo, on verra
En git sub module, on va dupliquer puis après créer une lib utilisée par les deux

4) Documentation => Comment on renseigne sur la démarche sur Gitlab 
Si c'est commun, c'est cool, y a un seul READme qui explique la démarche.
Si c'est des repos différents, ça va être galère, il faudra renvoyer à l'article.
Avec un repo et des submodules, ça pourrait faire le taff. On garde l'indépendance mais ils sont quand même liés.

#### On fait un repo de repo
CoP MFE pointe vers
-> monolith
-> micro-frontends

micro-frontends pointe vers
-> application-shell
-> presentation-valeur
-> creation-projet
-> suivi-projet
-> suivi-produit

## Alternatives

+ **On fait des Git submodules**
    + Docs centralisés dans le repo parent en haut
    + Partage de libraries à voir, on va faire de la duplication au départ
    + des cycles de vies découplés, vu que c'est des monorepo, chacun a ses commits et ses triggers de pipelines

+ **On fait pas monorepo**
  + Docs centralisés
  + Partage de libraries facilité
  - Des cycles de vie couplés, un commit sur le monorepo lance une pipeline et on devra faire des centaines de vérifications avant de faire des manips
  - 

+ **On fait pas multirepo**
  + des cycles de vies découplés
  + Partage de libs moisn faciles (publié des packages npm, ou bien dépendances sur git)
  - Docs décentralisés

## Actions

- [X] Créer le repo cop-micro-frontend avec docs dedans
- [X] Créer le repo cop-spa-monolith
- [X] Le relier à cop-micro-frontend
- Créer le repo cop-single-spa
- Créer le repo cop-single-spa-creation-projet
- Le relier au repo cop-mfe-single-spa